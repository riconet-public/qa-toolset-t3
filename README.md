# QS toolset for TYPO3 extensions

This package contains a collection of tools used for QA of TYPO3 extensions developed at PSV NEO GmbH.

## Contained tools

* phpunit
* php-cs-fixer
* phpstan
* phpmd
* typoscript-linter
* rector

## How to use

You can run the binary with the following options:

```text
Usage: $0 [options] [file]
    -t <qa>
        Specifies which tool to run
            - lint-php: Lints php files.
            - lint-code-style: Lints code style errors.
            - lint-typoscript: Lints typoscript files.
            - lint-debug: Lints for debug usage.
            - lint-feature-support: Checks if the code is ready for used TYPO3 and PHP version.
            - analyze: Analyzes php code.
            - qa: Checks general code quality.
            - fix-code-style: Fixes code style errors automatically.
            - fix-feature-support: Fixes code to be ready for used TYPO3 and PHP version.
            - unit: Runs unit tests.
            - functional: Runs functional tests.

    -p <8.1>
        Specifies the PHP minor version to be used
            - 8.1 (default): use PHP 8.1

    -c
        Path to custom config directory.
        This directory must contain every config files and the path must be absolute.

    -e
        Additional options and/or arguments to send to the specified tool.
        Example:
            -e "-v --filter canRetrieveValueWithGP"

    -i
        Shows information about the used tool.

    --help
        Show this help.

Examples:
    # Run code quality checks.
    .Build/bin/t3qa.sh -t lint-php -i
```
