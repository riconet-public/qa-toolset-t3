<?php

use Symplify\EasyCodingStandard\Config\ECSConfig;

return ECSConfig::configure()
    ->withPaths([__DIR__ . '/src'])
    ->withPreparedSets(
        psr12: true,
        common: true,
        symplify: true,
        cleanCode: true
    );