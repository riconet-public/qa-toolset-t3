<?php

/**
 * This file is part of the "qa-toolset-t3" software for PSV NEOs TYPO3 extension QA and CI.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

declare(strict_types=1);

namespace PSVNEO\QaToolsetT3\Command;

use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ToolRunnerCommand extends Command
{
    public const TOOL_COMMAND_MAP = [
        'lint:php' => 'lint-php',
        'lint:code-style' => 'lint-code-style',
        'lint:typoscript' => 'lint-typoscript',
        'lint:debug' => 'lint-debug',
        'lint:yaml' => 'lint-yaml',
        'refactoring:rector1' => 'refactoring-rector1',
        'refactoring:rector2' => 'refactoring-rector2',
        'refactoring:fractor' => 'refactoring-fractor',
        'analyze' => 'analyze',
        'qa' => 'qa',
        'test:unit' => 'test-unit',
        'test:functional' => 'test-functional',
        'fix:code-style' => 'fix-code-style',
    ];

    protected static $defaultName = 't3qa';

    protected static $defaultDescription = 'QA tool runner for TYPO3 extensions.';

    private string $tool = '';

    private string $phpVersion = '';

    public function __construct(
        private readonly string $projectRootPath,
        private readonly string $packageRoot
    ) {
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->addOption('tool', 't', InputOption::VALUE_OPTIONAL, 'Tool to execute. See help for full list.', 'qa');
        $this->addOption('php', 'p', InputOption::VALUE_OPTIONAL, 'Php version to use.', '8.2');
        $this->addOption('extra', 'e', InputOption::VALUE_OPTIONAL, 'Extra tool options/arguments.', '');
        $this->addOption('config', 'c', InputOption::VALUE_OPTIONAL, 'Custom config directory path.', '');
        $this->addOption('info', 'i', InputOption::VALUE_OPTIONAL, 'Shows information about the used tool.', false);
        $this->setHelp(<<<HELP
                -t <qa>
                    Specifies which tool to run
                        - lint:php: Lints php files (syntax).
                        - lint:code-style: Lints php files (code style / formatting).
                        - lint:typoscript: Lints typoscript files.
                        - lint:debug: Lints for debug usage.
                        - lint:yaml: Lints YAML files.
                        - refactoring:rector1: Uses rector in version 1 to check, if the code is ready for the current TYPO3 version.
                        - refactoring:recctor2: Uses rector in version 2 to check, if the code is ready for the current TYPO3 version.
                        - refactoring:fractor: Uses fractor to check, if the code is ready for the current TYPO3 version.
                        - analyze: Analyzes php code.
                        - qa: Checks general code quality.
                        - test:unit: Runs unit tests using phpunit.
                        - test:functional: Runs functional tests using phpunit and sqlite.
                        - fix:code-style: Fixes php files (code style / formatting).

                -p <8.2>
                    Specifies the PHP minor version to be used
                        - 8.2 (default): use PHP 8.2

                -e
                    Additional options and/or arguments to send to the specified tool.
                    Example:
                        -e "-v --filter canRetrieveValueWithGP"

                -i
                    Shows information about the used tool.

                --help
                    Show this help.
        HELP);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $this->tool = trim(strval($input->getOption('tool')));
            $this->phpVersion = trim(strval($input->getOption('php')));
            $showInfo = $input->getOption('info') !== false;
            $extraArgument = trim(strval($input->getOption('extra')));

            $io->title('PSV NEO - TYPO3 extension QA tool runner');

            $this->validateToolOption();
            $this->tool = self::TOOL_COMMAND_MAP[$this->tool];

            if ($showInfo) {
                $this->renderInfoTable($io);
            }

            // Start docker depending on the selected tool.
            $process = Process::fromShellCommandline("docker compose run --rm {$this->tool}", $this->packageRoot, [
                'ROOT_DIR' => $this->projectRootPath,
                'BIN_DIR' => $this->projectRootPath . '/.Build/bin/',
                'PACKAGE_ROOT_PATH' => $this->packageRoot,
                'PHP_VERSION' => $this->phpVersion,
                'EXTRA_TEST_OPTIONS' => $extraArgument,
            ]);
            $process->setTimeout(120);
            $process->run();
            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            $io->write($this->stripDockerMessages($process->getOutput()));
            $io->success(sprintf('"%s" has been executed with out errors.', $this->tool));

            return self::SUCCESS;
        } catch (ProcessFailedException $exception) {
            $io->write($this->stripDockerMessages($exception->getProcess()->getErrorOutput()));
            $io->write($this->stripDockerMessages($exception->getProcess()->getOutput()));
            $io->error('Exit with error. See above.');
        } catch (Exception $exception) {
            $io->error($exception->getMessage());
        } finally {
            Process::fromShellCommandline('docker compose down', $this->packageRoot)->run();
        }

        return self::FAILURE;
    }

    private function renderInfoTable(SymfonyStyle $io): void
    {
        $io->table([], [
            ['Working directory', $this->projectRootPath],
            ['Tool', $this->tool],
            ['PHP version', $this->phpVersion],
        ]);
    }

    private function validateToolOption(): void
    {
        if (! in_array($this->tool, array_keys(self::TOOL_COMMAND_MAP), true)) {
            throw new InvalidArgumentException(sprintf(
                "The specified tool \"%s\" does not exist. Allowed options are:\n%s",
                $this->tool,
                implode("\n", array_keys(self::TOOL_COMMAND_MAP))
            ));
        }
    }

    private function stripDockerMessages(string $output): string
    {
        $output = str_replace("Network qa-toolset-t3_default  Creating\n", '', $output);

        return str_replace("Network qa-toolset-t3_default  Created\n", '', $output);
    }
}
